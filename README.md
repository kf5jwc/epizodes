# Epizodes

Convert any YouTube channel into a downloadable playlist

## How to use `epizodes`

### 1. Head over to YouTube in your browser, and find the URL of your favorite user, channel, or playlist.

The URL will look like one of these:

+ `https://m.youtube.com/user/latenight`
+ `https://www.youtube.com/channel/UCVTyTA7-g9nopHeHbeuvpRA`
+ `https://www.youtube.com/playlist?list=PLUl4u3cNGP61Oq3tWYp6V_F-5jb5L2iHb`

### 2. Open up your podcasts app and add a new podcast by URL. Copy and paste in the URL from step 1, replacing the domain with your `epizodes` install.

Your modified URL should look like one of these:

+ `https://epizodes.example.com/user/latenight`
+ `https://epizodes.example.com/channel/UCVTyTA7-g9nopHeHbeuvpRA`
+ `https://epizodes.example.com/playlist?list=PLUl4u3cNGP61Oq3tWYp6V_F-5jb5L2iHb`

Epizodes generates audio podcasts by default. If you'd like a video podcast instead, simply add `?vid=1` to the end of the URL.

+ `https://epizodes.example.com/user/latenight?vid=1`
+ `https://epizodes.example.com/channel/UCVTyTA7-g9nopHeHbeuvpRA?vid=1`
+ `https://epizodes.example.com/playlist?list=PLUl4u3cNGP61Oq3tWYp6V_F-5jb5L2iHb&vid=1`

### 3. Hit subscribe. You're all set. You can now download and refresh episodes, just like with any other podcast.

## Setup

You'll need to place your YouTube API in the environment variable `YOUTUBE_API_KEY`
