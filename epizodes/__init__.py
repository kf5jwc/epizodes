import os
from loguru import logger
from cachetools import TTLCache
from collections import namedtuple


YOUTUBE_API_KEY = os.getenv("YOUTUBE_API_KEY")

if not YOUTUBE_API_KEY:
    raise EnvironmentError("YOUTUBE_API_KEY must be set!")

# Youtube API URL
YOUTUBE_API_ENDPOINT = os.getenv(
    "YOUTUBE_API_ENDPOINT", "https://www.googleapis.com/youtube/v3/"
)
logger.trace("YOUTUBE_API_ENDPOINT", YOUTUBE_API_ENDPOINT)

__YOUTUBE_API_CACHE_SIZE = os.getenv("YOUTUBE_API_CACHE_SIZE", 10000)

try:
    __YOUTUBE_API_CACHE_SIZE = int(__YOUTUBE_API_CACHE_SIZE)
except:
    raise EnvironmentError("YOUTUBE_API_CACHE_SIZE must be an integer!")
logger.trace("YOUTUBE_API_CACHE_SIZE", __YOUTUBE_API_CACHE_SIZE)


__YOUTUBE_API_CACHE_TTL = os.getenv("YOUTUBE_API_CACHE_TTL", 3600)

try:
    __YOUTUBE_API_CACHE_TTL = int(__YOUTUBE_API_CACHE_TTL)
except:
    raise EnvironmentError("YOUTUBE_API_CACHE_SIZE must be an integer!")
logger.trace("YOUTUBE_API_CACHE_TTL", __YOUTUBE_API_CACHE_TTL)

# Our cache
YOUTUBE_API_CACHE: TTLCache = TTLCache(
    maxsize=__YOUTUBE_API_CACHE_SIZE, ttl=__YOUTUBE_API_CACHE_TTL
)

# Youtube video URL
BASE_VIDEO_URL = "https://youtu.be/"

_FILETYPE = namedtuple("_FILETYPE", ["audio", "video"])
FILETYPE = _FILETYPE(audio="m4a", video="mp4")

from .views import app as application
