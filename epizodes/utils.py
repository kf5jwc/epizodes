import json
import isodate
import email.utils
from loguru import logger
from dateutil.parser import parse as parse_datetime
from starlette.requests import Request
from jinja2 import Environment, PackageLoader, select_autoescape
from urllib.parse import urlencode
from urllib.request import urlopen
from cachetools import cached
from . import YOUTUBE_API_KEY, YOUTUBE_API_ENDPOINT, YOUTUBE_API_CACHE, FILETYPE


# Our templates
templates = Environment(
    loader=PackageLoader("epizodes", "templates"),
    autoescape=select_autoescape(["html", "xml"]),
)

# Our statics
statics = Environment(loader=PackageLoader("epizodes", "statics"), autoescape=True)


def render_index() -> str:
    index = statics.get_template("index.html")
    return index.render()


def reformat_date_iso_to_rfc2822(date_iso: str) -> str:
    """Reformat publication dates for RSS (RFC 2822)"""
    parsed = parse_datetime(date_iso)
    date_rfc2822 = email.utils.format_datetime(parsed)
    return date_rfc2822


def reformat_file_duration_iso_to_itunes(duration_iso: str) -> str:
    """Reformat file duration for iTunes"""
    parsed = isodate.parse_duration(duration_iso)
    return str(parsed)


async def get_video_data(playlist_items):
    video_ids: [str] = [
        item["snippet"]["resourceId"]["videoId"] for item in playlist_items
    ]
    logger.trace("video_ids", video_ids)
    videos_data = await yt_api_call(
        "videos", "snippet,contentDetails", "id", ",".join(video_ids)
    )
    for item in videos_data["items"]:
        item["snippet"]["publishedAt"] = reformat_date_iso_to_rfc2822(
            item["snippet"]["publishedAt"]
        )
        item["contentDetails"]["duration"] = reformat_file_duration_iso_to_itunes(
            item["contentDetails"]["duration"]
        )
    return videos_data


async def render_feed(playlist_data, video: bool, request: Request) -> str:
    """Render the RSS feed from the playlist data, deciding if this is a video feed."""

    channel = await get_channel_data(playlist_data["items"][0]["snippet"]["channelId"])
    logger.debug(channel)
    videos = await get_video_data(playlist_data["items"])
    logger.debug(videos)

    template = templates.get_template("feed.xml.j2")
    return template.render(
        server=request.url,
        channel=channel["items"][0],
        videos=videos["items"],
        video=video,
        filetype=FILETYPE,
    )


async def get_channel_data(channel_id: str):
    """Return a dictionary of channel data from a channel id or username."""
    return await yt_api_call("channels", "contentDetails,snippet", "id", channel_id)


async def get_playlist_data(playlist_id: str):
    """Return a dictionary of playlist data from a playlist id."""
    return await yt_api_call("playlistItems", "snippet", "playlistId", playlist_id)


async def yt_api_call(path, part, id_type, id_value):
    """Make a call to the Youtube API"""

    params = {"key": YOUTUBE_API_KEY, id_type: id_value, "part": part, "maxResults": 50}

    data_url = YOUTUBE_API_ENDPOINT + path + "?" + urlencode(params)
    json_data = _fetch_external(data_url)
    return json.loads(json_data)


@cached(YOUTUBE_API_CACHE)
def _fetch_external(url):
    if "file:/" in url:
        raise ValueError("file:// scheme not allowed")

    with urlopen(url) as response:  # nosec
        return response.read()
