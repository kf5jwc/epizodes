import pafy
from loguru import logger
from starlette.responses import Response
from starlette.requests import Request
from fastapi import FastAPI
from urllib.error import HTTPError
from .utils import (
    get_channel_data,
    get_playlist_data,
    render_index,
    yt_api_call,
    render_feed,
)
from . import BASE_VIDEO_URL, FILETYPE


# Our app
app = FastAPI()


@app.get("/watch")
async def extract_playlist(list: str, vid: bool = False) -> Response:
    """Extract the list ID from '/watch?v=&list=' urls"""
    return Response(
        status_code=301, headers={"Location": f"/playlist?list={list}&vid={vid}"}
    )


@app.get("/user/{username}")
async def user_to_channel(username: str, vid: bool = False) -> Response:
    """Translate usernames into IDs for consistency."""
    logger.debug(f"Translating {username}")
    channel_info = await yt_api_call("channels", "id", "forUsername", username)
    try:
        channel_id = channel_info["items"][0]["id"]

    except IndexError:
        return Response(
            content=f"This may not be a valid user: {username}", status_code=404
        )

    else:
        return Response(
            status_code=301, headers={"Location": f"/channel/{channel_id}?vid={vid}"}
        )


@app.get("/channel/{channel_id}")
async def channel_feed(channel_id: str, vid: bool = False) -> Response:
    """Create an RSS feed for a channel"""

    channel_data = await get_channel_data(channel_id)

    try:
        uploads_playlist_id = channel_data["items"][0]["contentDetails"][
            "relatedPlaylists"
        ]["uploads"]

    except IndexError:
        return Response(
            content=f"This may not be a valid channel: {channel_id}", status_code=404
        )

    else:
        return Response(
            status_code=301,
            headers={"Location": f"/playlist?list={uploads_playlist_id}&vid={vid}"},
        )


@app.get("/playlist")
async def playlist_feed(
    list: str, vid: bool = False, request: Request = None
) -> Response:
    """Create an RSS feed from a YouTube playlist"""

    try:
        playlist_data = await get_playlist_data(list)

    except HTTPError:
        return Response(
            content=f"This may not be a valid playlist: {list}", status_code=404
        )

    feed = await render_feed(playlist_data, vid, request)
    return Response(
        content=feed, headers={"Content-Type": "application/rss+xml; charset=UTF-8"}
    )


@app.get("/download/{video_id}\\.{container}")
async def download(video_id: str, container: str) -> Response:
    """Determine the youtube stream location and redirect the client for download."""
    if container not in FILETYPE:
        return Response(status_code=415)

    try:
        video = pafy.new(BASE_VIDEO_URL + video_id)
    except ValueError:
        return Response(status_code=404)

    if container == FILETYPE.audio:
        stream = video.getbestaudio(preftype=container)
    elif container == FILETYPE.video:
        stream = video.getbeSt(preftype=container)

    if stream is None:
        return Response(status_code=404)

    return Response(status_code=302, headers={"Location": stream.url})


@app.get("/download/video/{video_id}\\.{container}", deprecated=True)
@app.get("/download/audio/{video_id}\\.{container}", deprecated=True)
async def deprecated_download(video_id: str, container: str) -> Response:
    """Use the /download/ endpoint instead."""
    return Response(
        status_code=301, headers={"Location": f"/download/{video_id}.{container}"}
    )


@app.get("/favicon.ico", include_in_schema=False)
def favicon() -> Response:
    return Response(status_code=404)


@app.get("/{username}")
async def root_user_to_channel(username: str, vid: bool = False) -> Response:
    """Handle custom URL paths like '/coldplayvevo'."""
    return Response(
        status_code=301, headers={"Location": f"/user/{username}?vid={vid}"}
    )


@app.get("/", include_in_schema=False)
async def index() -> str:
    return Response(content=render_index(), media_type="text/html")
